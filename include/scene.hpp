#ifndef SCENE_HPP
#define SCENE_HPP

#include <memory>
#include <vector>

#include <mutex>

namespace TE {

    class Canvas;
    typedef std::shared_ptr<Canvas> CanvasPtr;

    class Object;
    typedef std::shared_ptr<Object> ObjectPtr;

    /**
     * @brief an object of this class allow you to manage graphic objects on the screen
     * This is not a copyable object
     */
    class Scene {

        /**
         * @brief object for sync operations with the scene 
         * 
         */
        mutable std::recursive_mutex m_sceneLock;

        /**
         * @brief object displays graphic objects
         * 
         */
        CanvasPtr m_canvas;

        /**
         * @brief storage for graphic objects 
         * 
         */
        std::vector<ObjectPtr> m_objects;

    public:

        /**
         * @brief Construct a new Scene object
         * 
         * @param width scene width
         * @param height scene height
         */
        Scene(int width, int height);

        /**
         * @brief Destroy the Scene object
         * 
         */
        ~Scene() = default;

        Scene(const Scene&) = delete;
        Scene(Scene&&) = delete;

        Scene& operator=(const Scene&) = delete;
        Scene& operator=(Scene&&) = delete;

        /**
         * @brief this method allows you to add a graphic object to the screen
         * 
         * @param object a pointer to the graphic object
         * @return true - adding an object to the scene succeded
         * @return false - adding an object to the scene failed
         */
        bool appendObject(const ObjectPtr& object);

        /**
         * @brief this method allows you to remove an object from the scene by its index in the storage
         * 
         * @param index an object index in storage
         */
        void removeObject(size_t index);

        /**
         * @brief this method allows you to remove an object from the scene
         * 
         * @param object pointer to the removing object
         */
        void removeObject(const ObjectPtr& object);

        /**
         * @brief this method allows you to check that an object is in the scene
         * 
         * @param object pointer to an object you want to check
         * @return true - the object is in the scene 
         * @return false - the object isn't in the scene
         */
        bool hasObject(const ObjectPtr& object) const;

        /**
         * @brief get the objects number in the scene 
         * 
         * @return size_t - size of the object storage
         */
        size_t getObjectsNum() const;

        /**
         * @brief get the screen width
         * 
         * @return int - screen width value
         */
        int getWidth() const;

        /**
         * @brief get the screen height
         * 
         * @return int - screen height value
         */
        int getHeight() const;

        /**
         * @brief update all object in the scene
         * 
         * @param FPS current FPS value
         */
        void update(clock_t FPS);

    private:

        /**
         * @brief draw graphic objects
         * 
         */
        void draw();

    };

}

#endif // !SCENE_HPP