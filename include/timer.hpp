#ifndef TIMER_HPP
#define TIMER_HPP

#include <thread>
#include <functional>

#include <time.h>
#include <chrono>

namespace TE {

    class Timer {

        /**
         * @brief time is seconds
         * 
         */
        uint64_t m_time;

        /**
         * @brief timer is looped 
         * 
         */
        bool m_loop;

        /**
         * @brief timer ticking thread
         * 
         */
        std::thread m_timerThread;

        /**
         * @brief time is over callback
         * 
         */
        std::function<void()> m_callback;

        /**
         * @brief current time value
         * 
         */
        uint64_t m_currentTime;

        /**
         * @brief timer is started
         * 
         */
        bool m_isStarted;

    public:

        /**
         * @brief Construct a new Timer object
         * 
         * @param time time to wait
         * @param callback callback to invoke when time is over
         * @param loop timer is looped
         * @param start start timer after construct
         */
        Timer(uint64_t time, const std::function<void()>& callback, bool loop = false, bool start = false);
        ~Timer();

        /**
         * @brief start the timer
         * 
         */
        void start();

        /**
         * @brief stop the timer
         * 
         */
        void stop();

    private:

        /**
         * @brief timer ticking function
         * 
         */
        void tick();

    };

}

#endif //!TIMER_HPP