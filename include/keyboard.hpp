#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP

#include <cassert>
#include <cstring>
#include <thread>

#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

namespace TE {

    enum class KeyState : char {
        Undefined = -1,
        Pressed = 2,
        Released = 3
    };

    /**
     * @brief an object of this class allows you to grab keyboard events
     * This is not copyable object 
     * 
     */
    class Keyboard {

        /**
         * @brief X11 display
         * 
         */
        Display* m_display;

        //TODO: don't use the root window anymore
        //NOTE: it blocks user input until the app is closed
        Window m_window;

        /**
         * @brief current key
         * 
         */
        static int m_key;

        /**
         * @brief current key state
         * 
         */
        static KeyState m_keyState;

        /**
         * @brief is listening
         * 
         */
        static bool m_isStarted;

        /**
         * @brief listener thread
         * 
         */
        std::thread m_listener;

    public:

        /**
         * @brief Construct a new Keyboard object
         * 
         */
        Keyboard() = default;

        /**
         * @brief Destroy the Keyboard object
         * 
         */
        ~Keyboard();

        Keyboard(const Keyboard&) = delete;
        Keyboard(Keyboard&&) = delete;

        Keyboard& operator=(const Keyboard&) = delete;
        Keyboard& operator=(Keyboard&&) = delete;

        /**
         * @brief start listener thread
         * 
         */
        void start();

        /**
         * @brief stop listener thread
         * 
         */
        void stop();

        /**
         * @brief check the key is pressed
         * 
         * @param code key code
         * @return true - key is pressed
         * @return false - key is not pressed
         */
        static bool isKeyPressed(int code);

        /**
         * @brief check the key is released
         * 
         * @param code key code
         * @return true - key is released
         * @return false - ket is not released
         */
        static bool isKeyReleased(int code);

private:

        /**
         * @brief listener loop
         * 
         */
        void loop();

    };

}

#endif // !KEYBOARD_HPP