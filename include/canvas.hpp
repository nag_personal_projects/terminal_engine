#ifndef CANVAS_HPP
#define CANVAS_HPP

#include "image.hpp"

namespace TE {

	/**
	 * @brief an object of this class allows you to change a "pixel" value on the screen
	 * 
	 */
	class Canvas : public Image {

		/**
		 * @brief line buffer
		 * it needs to display canvas data line by line
		 * 
		 */
		char* m_buffer;

	public:

		/**
		 * @brief Construct a new Canvas object
		 * 
		 * @param width of screen
		 * @param height of screen
		 */
		Canvas(int width, int height);

		/**
		 * @brief Construct a new Canvas object
		 * 
		 * @param canvas object for copying
		 */
		Canvas(const Canvas& canvas);

		/**
		 * @brief Construct a new Canvas object
		 * 
		 * @param canvas object for moving
		 */
		Canvas(Canvas&& canvas);

		/**
		 * @brief Construct a new Canvas object
		 * 
		 * @param image for making canvas
		 */
		Canvas(const Image& image);

		/**
		 * @brief Destroy the Canvas object
		 * 
		 */
		~Canvas();

		/**
		 * @brief get const 'pixel' at (x, y) coords
		 * 
		 * @param x coord
		 * @param y coord
		 * @return const char& - 'pixel' value
		 */
		const char& at(int x, int y) const;
		
		/**
		 * @brief get const 'pixel' at (x, y) coords
		 * 
		 * @param x coord
		 * @param y coord
		 * @return char& - 'pixel' value
		 */
		char& at(int x, int y);

		/**
		 * @brief this method allows you to clean the canvas data
		 * 
		 */
		void clear();

		/**
		 * @brief this method allows you to display the canvas data on the screen
		 * 
		 */
		void display() const;

		Canvas& operator=(const Canvas& canvas);
		Canvas& operator=(Canvas&& canvas);

		Canvas& operator=(const Image& image);

	};

}

#endif // !CANVAS_HPP
