#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <memory>
#include <string>

namespace TE {

    class Image;
    typedef std::shared_ptr<Image> ImagePtr;

    class Image {

    protected:

        /**
         * @brief image width
         * 
         */
        int m_width;

        /**
         * @brief image height
         * 
         */
        int m_height;

        /**
         * @brief image data
         * 
         */
        char* m_data;

    public:

        /**
         * @brief Construct a new Image object
         * 
         */
        Image();

        /**
         * @brief Construct a new Image object
         * 
         * @param width image width
         * @param height image height
         * @param data image data
         */
        Image(int width, int height, const char* data = nullptr);

        /**
         * @brief Construct a new Image object
         * 
         * @param image image for copying
         */
        Image(const Image& image);

        /**
         * @brief Construct a new Image object
         * 
         * @param image image for moving
         */
        Image(Image&& image);

        /**
         * @brief Destroy the Image object
         * 
         */
        ~Image();

        /**
         * @brief set the width to the image
         * 
         * @param width new width value
         */
        void setWidth(int width);

        /**
         * @brief get the width value of the image
         * 
         * @return int - width value
         */
        int getWidth() const;

        /**
         * @brief set the height to the image
         * 
         * @param height new height value
         */
        void setHeight(int height);

        /**
         * @brief get the height of the image
         * 
         * @return int - height value
         */
        int getHeight() const;

        /**
         * @brief change size value of the image
         * 
         * @param width new width value
         * @param height new height value
         */
        void resize(int width, int height);

        /**
         * @brief get the image pixel value
         * 
         * @param x coord
         * @param y coord
         * @return const char& - const pixel value
         */
        const char& at(int x, int y) const;

        /**
         * @brief get the image pixel value
         * 
         * @param x coord
         * @param y coord
         * @return char& - pixel value
         */
        char& at(int x, int y);

        /**
         * @brief get image data
         * 
         * @return const char* - const image data
         */
        const char* getData() const;

        /**
         * @brief get image data
         * 
         * @return char* - image data
         */
        char* getData();

        /**
         * @brief clear an image
         * 
         */
        void clear();

        /**
         * @brief save image to file
         * 
         * @param path path to file
         * @return true - image saved
         * @return false - image is not saved
         */
        bool saveToFile(const std::string& path);

        /**
         * @brief load image from file
         * 
         * @param path path to file
         * @return true - image loaded
         * @return false - image is not loaded
         */
        bool loadFromFile(const std::string& path);

        /**
         * @brief compare two images
         * 
         * @param image object to compare
         * @return true - object are equal
         * @return false - object are not equal
         */
        bool compare(const Image& image) const;

        /**
         * @brief create an image objet from a file
         * 
         * @param path path to file
         * @return ImagePtr - pointer to create image object
         */
        static ImagePtr createFromFile(const std::string& path);

        Image& operator=(const Image& image);
        Image& operator=(Image&& image);

        bool operator==(const Image& image) const;
        bool operator!=(const Image& image) const;

    };

}

#endif //!IMAGE_HPP