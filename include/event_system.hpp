#ifndef EVENT_SYSTEM_HPP
#define EVENT_SYSTEM_HPP

#include <memory>
#include <vector>

namespace TE {

    class Object;

    /**
     * @brief types of events
     * 
     */
    enum class EventType : char {
        Undefined = -1,
        Created = 0,            // object created
        Destroyed = 1          // object destroyed
        // Removed = 2             // child of an object destroyed
    };

    /**
     * @brief event object
     * 
     */
    struct Event {
        /**
         * @brief event type
         * 
         */
        EventType type;

        /**
         * @brief object that sends the event
         * 
         */
        Object* sender;

        /**
         * @brief changed object
         * 
         */
        Object* object;

        /**
         * @brief Construct a new Event object
         * 
         */
        Event() {
            type = EventType::Undefined;
        }

        /**
         * @brief Construct a new Event object
         * 
         * @param s sender object
         * @param o changed obejct
         * @param type event type
         */
        Event(Object* s,
              Object* o,
              EventType type) :
              sender(s),
              object(o)
        {
            this->type = type;
        }
    };

    typedef std::shared_ptr<Event> EventPtr;

    /**
     * @brief event system (observer)
     * 
     */
    class EventSystem {

        /**
         * @brief objects to be notified when event occurs 
         * 
         */
        std::vector<Object*> m_subs;

    public:

        /**
         * @brief Construct a new Event System object
         * 
         */
        EventSystem() = default;

        /**
         * @brief Destroy the Event System object
         * 
         */
        ~EventSystem() = default;

        EventSystem(const EventSystem&) = delete;
        EventSystem(EventSystem&&) = delete;

        EventSystem& operator=(const EventSystem&) = delete;
        EventSystem& operator=(EventSystem&&) = delete;

        /**
         * @brief add listener
         * 
         * @param sub listener object
         */
        void appendSubscriber(Object* sub);
        
        /**
         * @brief remove listener by its index
         * 
         * @param index index of listener
         */
        void removeSubscriber(size_t index);

        /**
         * @brief remove listener
         * 
         * @param sub listener object
         */
        void removeSubscriber(Object* sub);

        /**
         * @brief get number of listeners
         * 
         * @return size_t - number of listeners value
         */
        size_t getNumSubscribers() const;

        /**
         * @brief send event to all listeners
         * 
         * @param event event object
         */
        void send(const EventPtr& event);

    };

}

#endif // !EVENT_SYSTEM_HPP