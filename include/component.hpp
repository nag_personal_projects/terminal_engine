#ifndef COMPOMENT_HPP
#define COMPOMENT_HPP

#include <memory>
#include <string>

namespace TE {

    class Object;

    // There is no sense to create an object of this class manually. 
    // Only objects of TE::Object class must create components.

    class Component {

        friend class Object;

    protected:

        /**
         * @brief parent object of the component
         * 
         */
        Object* m_parent;

        /**
         * @brief component name
         * 
         */
        std::string m_name;

    public:

        /**
         * @brief Construct a new Compoment object
         * 
         * @param parent the owner of the component
         */
        Component(Object* parent);

        /**
         * @brief Destroy the Component object
         * 
         */
        virtual ~Component() = default;

        Component(const Component&) = delete;
        Component(Component&&) = delete;

        Component& operator=(const Component&) = delete;
        Component& operator=(Component&&) = delete;

        /**
         * @brief Get the Parent object
         * 
         * @return ObjectPtr_w - parent object
         */
        Object* getParent() const;

        /**
         * @brief get the name of the component
         * 
         * @return const std::string& - component name
         */
        const std::string& getName() const;

    private:

        /**
         * @brief update state of a components
         * 
         * @param FPS - current FPS value
         */
        virtual void update(clock_t FPS) = 0;

    };

};

#endif //!COMPOMENT_HPP