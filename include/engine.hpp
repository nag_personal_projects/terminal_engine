#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <memory>
#include <thread>
#include <ctime>

namespace TE {

	class Scene;
	typedef std::shared_ptr<Scene> ScenePtr;

	class Keyboard;
	typedef std::shared_ptr<Keyboard> KeyboardPtr;

	class Engine {

		/**
		 * @brief engine is running
		 * 
		 */
		bool m_isRunning;

		/**
		 * @brief thread executes the main loop
		 * 
		 */
		std::thread m_loopThread;

		/**
		 * @brief current FPS value
		 * 
		 */
		clock_t m_fps;

		/**
		 * @brief value of FPS limit
		 * 
		 */
		int m_fpsLimit;

		/**
		 * @brief fps limited
		 * 
		 */
		bool m_isFPSLimited;

		/**
		 * @brief scene that renders all graphic objects
		 * 
		 */
		ScenePtr m_scene;

		/**
		 * @brief keyboard event listener
		 * 
		 */
		KeyboardPtr m_keyListener;

		/**
		 * @brief the name of the application
		 * 
		 */
		std::string m_appName;

	public:

		/**
		 * @brief Construct a new Engine object
		 * 
		 * @param appName application name
		 * @param scene scene object
		 */
		Engine(const std::string& appName = "", const ScenePtr& scene = nullptr);

		/**
		 * @brief Destroy the Engine object
		 * 
		 */
		~Engine();

		/**
		 * @brief run engine loop
		 * 
		 */
		void run();

		/**
		 * @brief stop engine loop
		 * 
		 */
		void stop();

		/**
		 * @brief set the limit of FPS value
		 * 
		 * @param limit new value
		 */
		void setLimitFPS(int limit);

		/**
		 * @brief limit FPS value
		 * 
		 * @param value need to limit FPS
		 */
		void limitFPS(bool value);

		/**
		 * @brief is FPS limited
		 * 
		 * @return true - FPS is limited
		 * @return false - FPS is unlimited
		 */
		bool isFPSLimited() const;

		/**
		 * @brief get current FPS value
		 * 
		 * @return clock_t - current FPS value
		 */
		clock_t getFPS() const;

		/**
		 * @brief is engine running
		 * 
		 * @return true - engine is running
		 * @return false - engine is not running
		 */
		bool isRunning() const;

		/**
		 * @brief set application name
		 * 
		 * @param appName new application name
		 */
		void setAppName(const std::string& appName);

		/**
		 * @brief get application name
		 * 
		 * @return const std::string& - application name value
		 */
		const std::string& getAppName() const;

		/**
		 * @brief set new scene object to the engine
		 * 
		 * @param scene scene object
		 */
		void setScene(const ScenePtr& scene);

		/**
		 * @brief get current scene object
		 * 
		 * @return const ScenePtr& - scene object
		 */
		const ScenePtr& getScene() const;

	private:

		/**
		 * @brief engine main loop
		 * 
		 */
		void loop();

	};

}

#endif // !ENGINE_HPP
