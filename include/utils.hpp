#ifndef UTILS_HPP
#define UTILS_HPP

namespace TE::Utils {

    /**
     * @brief check that two object intersect
     * 
     * @param x1 x coord of first object
     * @param y1 y coord of second object
     * @param w1 width value of first object
     * @param h1 height value of first object
     * @param x2 x coord of second object
     * @param y2 y coord of second object
     * @param w2 width value of second object
     * @param h2 height value of second object
     * @return true - object intersect
     * @return false - object don't intersect
     */
    bool isIntersected(int x1, int y1, int w1, int h1,
                       int x2, int y2, int w2, int h2);

    /**
     * @brief compare two float values
     * 
     * @param a first value
     * @param b second value
     * @return true - values are equal
     * @return false - values aren't equal
     */
    bool compare(float a, float b);

}

#endif // !UTILS_HPP