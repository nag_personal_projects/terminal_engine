#ifndef OBJECT_H
#define OBJECT_H

#include <algorithm>
#include <memory>
#include <vector>

#include "component.hpp"

namespace TE {

    class Canvas;
    typedef std::shared_ptr<Canvas> CanvasPtr;

    class Object;
    typedef std::shared_ptr<Object> ObjectPtr;

    class EventSystem;
    typedef std::shared_ptr<EventSystem> EventSystemPtr;

    struct Event;
    typedef std::shared_ptr<Event> EventPtr;

    class Component;
    typedef std::shared_ptr<Component> ComponentPtr;

    class Image;
    typedef std::shared_ptr<Image> ImagePtr;

    /**
     * @brief base class for an object that can be drawn on the screen
     * 
     */
    class Object {

        /**
         * @brief this is necessary, that the private 'onEvent' method
         * can be called by an object of the EventSystem class
         */
        friend class EventSystem;

        /**
         * @brief the unique id of ab objects
         * 
         */
        size_t m_id;

        /**
         * @brief the mark that an object is destroyed
         * 
         */
        bool m_isDestroyed;

        /**
         * @brief the mark that an object is visible or not
         * 
         */
        bool m_isVisible;

        /**
         * @brief an object's event system, allowing it to notify
         * other object of a state change
         */
        EventSystemPtr m_eventSystem;

        /**
         * @brief object texture
         * 
         */
        ImagePtr m_texture;

        /**
         * @brief object components
         * 
         */
        std::vector<ComponentPtr> m_components;

    protected:

        /**
         * @brief object width
         * 
         */
        int m_width;

        /**
         * @brief object height
         * 
         */
        int m_height;

        /**
         * @brief object pos X
         * 
         */
        float m_posX;

        /**
         * @brief object pos Y
         * 
         */
        float m_posY;

        /**
         * @brief tag list of an object
         * 
         */
        std::vector<std::string> m_tags;

    public:

        /**
         * @brief Construct a new Object object
         * 
         * @param width object width
         * @param height object height
         * @param x pos X
         * @param y pos Y
         */
        Object(int width, int height, float x, float y);

        /**
         * @brief Destroy the Object object
         * 
         */
        virtual ~Object() = default;

        /**
         * @brief get object id
         * 
         * @return size_t - id value
         */
        size_t getID() const;

        /**
         * @brief set new texture to object
         * 
         * @param texture - image object
         */
        void setTexture(const ImagePtr& texture);

        /**
         * @brief get current texture of the object 
         * 
         * @return const ImagePtr& - image object
         */
        const ImagePtr& getTexture() const;

        /**
         * @brief set the width of an object
         * 
         * @param width new width value
         */
        void setWidth(int width);

        /**
         * @brief get the width of an object
         * 
         * @return int - width value
         */
        int getWidth() const;

        /**
         * @brief set the height of an object
         * 
         * @param height new height value
         */
        void setHeight(int height);

        /**
         * @brief get the height of an object
         * 
         * @return int - height value
         */
        int getHeight() const;

        /**
         * @brief set the pos X of an object
         * 
         * @param x new value of the X coord
         */
        void setPosX(float x);

        /**
         * @brief get the pos X of an object
         * 
         * @return float - X coord value
         */
        float getPosX() const;

        /**
         * @brief set the pos Y of an object
         * 
         * @param y new value of the Y coord
         */
        void setPosY(float y);

        /**
         * @brief get the pos Y of an object
         * 
         * @return float - Y coord value
         */
        float getPosY() const;

        /**
         * @brief set the size of an object
         * 
         * @param width new width value
         * @param height new height value
         */
        void setSize(int width, int height);

        /**
         * @brief set the position of an object
         * 
         * @param x new X coord value
         * @param y new Y coord value 
         */
        void setPosition(float x, float y);

        /**
         * @brief set new value to the Visible property of an object
         * 
         * @param visible visibility value
         */
        void setVisible(bool visible);

        /**
         * @brief get value of the Visible property of an object
         * 
         * @return true - object is visible
         * @return false - object is invisible
         */
        bool isVisible() const;

        /**
         * @brief get value of the Destroyed property of an object
         * 
         * @return true - object is destroyed
         * @return false - obejct is not destroyed
         */
        bool isDestroyed() const;

        /**
         * @brief append new tag to the tag list
         * 
         * @param tag new tag value
         */
        void appendTag(const std::string& tag);

        /**
         * @brief remove tag from the tag list by its value
         * 
         * @param tag the tag value
         */
        void removeTag(const std::string& tag);

        /**
         * @brief remove tag from the tag list by its index
         * 
         * @param index the tag index
         */
        void removeTag(size_t index);

        /**
         * @brief get the number of tags in the tag list
         * 
         * @return size_t - number of tags in the list
         */
        size_t getTagsNum() const;

        /**
         * @brief is there the tag in the tag list
         * 
         * @param tag tag value
         * @return true - the tag is in the list
         * @return false - the tag isn't in the list
         */
        bool hasTag(const std::string& tag) const;

        /**
         * @brief append component to object by its type
         * 
         * @tparam T - type of the component
         */
        template<typename T>
        std::shared_ptr<T> appendComponent() {
            for (auto component : m_components) {
                std::shared_ptr<T> found = std::dynamic_pointer_cast<T>(component);
                if (found != nullptr) {   
                    return found;
                }
            }

            ComponentPtr component = std::make_shared<T>(this);
            m_components.push_back(component);

            return std::dynamic_pointer_cast<T>(component);
        }   

        /**
         * @brief remove component from object by its type
         * 
         * @tparam T - type of the component
         */
        template<typename T>
        void removeComponent() {
            for (auto it = m_components.begin(); it != m_components.end(); ++it) {
                if (std::dynamic_pointer_cast<T>(*it)) {
                    m_components.erase(it);
                    return;
                }
            }
        }

        /**
         * @brief get component by its type
         * 
         * @tparam T - type of the component
         * @return ComponentPtr - component object or nullptr
         */
        template<typename T>
        std::shared_ptr<T> getComponent() {
            for (auto it = m_components.begin(); it != m_components.end(); ++it) {
                std::shared_ptr<T> component = std::dynamic_pointer_cast<T>(*it);
                if (component) {
                    return component;
                }
            }

            return nullptr;
        }

        /**
         * @brief get component by its index
         * 
         * @param index index of the component
         * @return ComponentPtr - component object of nullptr
         */
        ComponentPtr getComponent(size_t index) const;

        /**
         * @brief get number of components
         * 
         * @return size_t - number of components
         */
        size_t getComponentsNum() const;

        /**
         * @brief make an object as destroyed
         * 
         */
        void destroy();

        /**
         * @brief update object state
         * 
         * @param FPS current FPS value
         */
        void update(clock_t FPS);

        /**
         * @brief draw object on the screen
         * 
         * @param canvas canvas object
         */
        void draw(const CanvasPtr& canvas);

        /**
         * @brief object intersects with another object
         * 
         * @param object intersected object
         */
        void intersects(const ObjectPtr& object);

        /**
         * @brief get the event system of an object object
         * 
         * @return const EventSystemPtr& - event system object
         */
        const EventSystemPtr& getEventSystem() const;

    protected:

        /**
         * @brief destroy specific data of a specific object
         * 
         */
        virtual void onDestroy() = 0;

        /**
         * @brief update specific data of a specific object
         * 
         * @param FPS current FPS value
         */
        virtual void onUpdate(clock_t FPS) = 0;

        /**
         * @brief draw a specific object
         * 
         * @param canvas canvas for drawing an object
         */
        virtual void onDraw(const CanvasPtr& canvas) = 0;

        /**
         * @brief intersecting of two object
         * 
         * @param object obejct
         */
        virtual void onIntersects(const ObjectPtr& object) = 0;

        /**
         * @brief this method will be call, when some event happens
         * 
         * @param event event object
         */
        virtual void onEvent(const EventPtr& event) {}

    };

} 

#endif // !OBJECT_H