#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <memory>
#include <string>
#include <vector>

namespace TE {

    class Image;
    typedef std::shared_ptr<Image> ImagePtr;

    enum class AnimDirection : char {
        FORWARD = 0,
        BACKWARD = 1
    };

    class Animation {

        /**
         * @brief animation name
         * 
         */
        std::string m_name;

        /**
         * @brief playing state of an animation
         * 
         */
        bool m_isPlaying;

        /**
         * @brief loop state of an animation
         * 
         */
        bool m_isLooped;

        /**
         * @brief reversed state of an animation
         * can be played in reverse order
         * 
         */
        bool m_isReversed;

        /**
         * @brief interruptable state of an animation
         * looped animation can be interrupted
         * 
         */
        bool m_isInterruptable;

        /**
         * @brief direction of an animation
         * 
         */
        AnimDirection m_direction;

        /**
         * @brief animation playing time
         * 
         */
        int m_time;

        /**
         * @brief delay between animation frames
         * 
         */
        int m_delay;

        /**
         * @brief animation frames
         * 
         */
        std::vector<ImagePtr> m_frames;

        /**
         * @brief current frame of an animation
         * 
         */
        ImagePtr m_currentFrame;

        /**
         * @brief index of the current frame
         * 
         */
        int m_currentIndex;

    public:

        /**
         * @brief Construct a new Animation object
         * 
         * @param name animation name
         * @param delay delay between frames
         */
        Animation(const std::string& name, int delay = 5);

        /**
         * @brief Construct a new Animation object
         * 
         * @param animation object for copying
         */
        Animation(const Animation& animation);

        /**
         * @brief Construct a new Animation object
         * 
         * @param animation object for moving
         */
        Animation(Animation&& animation);

        /**
         * @brief Destroy the Animation object
         * 
         */
        ~Animation();

        /**
         * @brief set new name to an animation
         * 
         * @param name new name value
         */
        void setName(const std::string& name);

        /**
         * @brief get current name of an animation
         * 
         * @return const std::string& - name value
         */
        const std::string& getName() const;

        /**
         * @brief set loop state value
         * 
         * @param looped new loop value
         */
        void setLooped(bool looped);

        /**
         * @brief checking if an animation is lopped or not
         * 
         * @return true - animation is looped
         * @return false - animation is not looped
         */
        bool isLooped() const;

        /**
         * @brief set reversed state value
         * 
         * @param reversed new reversed value
         */
        void setReversed(bool reversed);

        /**
         * @brief checking if an animation is reversed or not
         * 
         * @return true - animation is reversed
         * @return false - animation is not reversed
         */
        bool isReversed() const;

        /**
         * @brief set new interruptable state value
         * 
         * @param interruptable - new state value
         */
        void setInterruptable(bool interruptable);

        /**
         * @brief checking if an animation can be interrupted at runtime
         * 
         * @return true - can be interrupted
         * @return false - cannot be interrupted
         */
        bool isInterruptable() const;

        /**
         * @brief set new direction value
         * 
         * @param direction new direction value
         */
        void setDirectionType(AnimDirection direction);

        /**
         * @brief get current direction value of an animation
         * 
         * @return AnimDirection - direction value
         */
        AnimDirection getDirectionType() const;

        /**
         * @brief set new delay between animation frames
         * 
         * @param delay new delay value
         */
        void setDelay(int delay);

        /**
         * @brief get current delay between animation frames
         * 
         * @return int - current delay value
         */
        int getDelay() const;

        /**
         * @brief checking if an animation is playing at some moment
         * 
         * @return true - animation is playing
         * @return false - animaton is not playing
         */
        bool isPlaying() const;

        /**
         * @brief append new frame to the animation
         * 
         * @param frame - image of new frame
         */
        void appendFrame(const ImagePtr& frame);

        /**
         * @brief remove frame
         * 
         * @param frame the frame that'll be removed
         */
        void removeFrame(const ImagePtr& frame);

        /**
         * @brief remove frame by its index
         * 
         * @param index - index of the frame, that'll be removed
         */
        void removeFrame(size_t index);

        /**
         * @brief get numbers of frames
         * 
         * @return size_t - number of frames value
         */
        size_t getFramesNum() const;

        /**
         * @brief get animation frame by its index
         * 
         * @param index the index of frame
         * @return ImagePtr - pointer to image object or nullptr
         */
        ImagePtr getFrame(size_t index) const;

        /**
         * @brief get current frame of an animation
         * 
         * @return const ImagePtr& - pointer to the image object
         */
        const ImagePtr& getCurrentFrame() const;

        /**
         * @brief play animation
         * 
         */
        void play();

        /**
         * @brief stop playing animation
         * 
         */
        void stop();

        /**
         * @brief change frame to next manually
         * 
         */
        void next();

        /**
         * @brief change frame to previous manually
         * 
         */
        void prev();

        /**
         * @brief auto-changing frames
         * 
         */
        void update();

        Animation& operator=(const Animation& animation);
        Animation& operator=(Animation&& animation);

    };

};

#endif //!ANIMATION_HPP