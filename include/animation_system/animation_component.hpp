#ifndef ANIMATION_COMPONENT_HPP
#define ANIMATION_COMPONENT_HPP

#include "component.hpp"

#include <map>
#include <memory>
#include <string>

namespace TE {

    class Animation;
    typedef std::shared_ptr<Animation> AnimationPtr;

    class AnimationComponent : public Component {

        /**
         * @brief animation storage
         * 
         */
        std::map<std::string, AnimationPtr> m_animations;

        /**
         * @brief current animation
         * 
         */
        AnimationPtr m_currentAnimation;

    public:

        /**
         * @brief Construct a new Animation Component object
         * 
         * @param parent pointer to parent(owner) object
         */
        AnimationComponent(Object* parent);

        /**
         * @brief Destroy the Animation Component object
         * 
         */
        ~AnimationComponent() override;

        /**
         * @brief append new animation
         * 
         * @param animation - pointer to an animation object
         */
        void appendAnimation(const AnimationPtr& animation);

        /**
         * @brief remove animation by its name
         * 
         * @param name the name of the animation to be removed
         */
        void removeAnimation(const std::string& name);

        /**
         * @brief get animation by its name
         * 
         * @param name the name of the animation
         * @return AnimationPtr - pointer to the animation object
         */
        AnimationPtr getAnimation(const std::string& name);

        /**
         * @brief set current animation
         * 
         * @param animation the animation object to be setted
         */
        void setCurrentAnimation(const AnimationPtr& animation);

        /**
         * @brief set current animation by its name
         * 
         * @param name the name of the animation to be set as current
         */
        void setCurrentAnimation(const std::string& name);

        /**
         * @brief get current animation
         * 
         * @return const AnimationPtr& - pointer to the current animation object
         */
        const AnimationPtr& getCurrentAnimation() const;

        /**
         * @brief play current animation
         * 
         */
        void play();

        /**
         * @brief stop playing current animation
         * 
         */
        void stop();

    private:

        /**
         * @brief update current animation
         * 
         * @param FPS current FPS value
         */
        void update(clock_t FPS) override;

    };


};

#endif //ANIMATION_COMPONENT_HPP