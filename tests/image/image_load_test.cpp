#include <cstring>

#include <image.hpp>

const std::string path = "./../../../resources/test_load_image.imgte";

const int WIDTH = 8;
const int HEIGHT = 8;
constexpr int SIZE = WIDTH * HEIGHT;
const char DATA[] = "*********##**##************##******##****#****#**######*********";

int main(int argc, char** argv) {
    puts(argv[0]);

    TE::ImagePtr image = TE::Image::createFromFile(path);
    if (image == nullptr) {
        return -1;
    }

    const int width = image->getWidth();
    const int height = image->getHeight();
    if (width != WIDTH || height != HEIGHT) {
        return -1;
    }

    const char* data = image->getData();
    return std::memcmp(data, DATA, SIZE) != 0;
}