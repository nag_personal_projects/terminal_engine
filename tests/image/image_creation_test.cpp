#include <image.hpp>

#include <cstring>

const int WIDTH = 4;
const int HEIGHT = 3;
const int SIZE = 12;

const char DATA[] = "*0*0*0*0*0*0";
const char EMPTY_DATA[] = "000000000000";

int main() {
    // default constructor (width = 0, height = 0, data = nullptr)
    TE::Image image;

    int width = image.getWidth();
    int height = image.getHeight();
    char* data = image.getData();

    if (width != 0 || height != 0) {
        return -1;
    }

    if (data != nullptr) {
        return -2;
    }

    // constructor (width, height, data = nullptr)
    image = TE::Image(WIDTH, HEIGHT);

    width = image.getWidth();
    height = image.getHeight();
    data = image.getData();

    if (width != WIDTH || height != HEIGHT) {
        return -1;
    }
    
    if (std::memcmp(EMPTY_DATA, data, SIZE) != 0) {
        return -2;
    }

    // constructor (width, height, data)
    image = TE::Image(WIDTH, HEIGHT, DATA);

    width = image.getWidth();
    height = image.getHeight();
    data = image.getData();

    if (width != WIDTH || height != HEIGHT) {
        return -1;
    }

    if (std::memcmp(DATA, data, SIZE) != 0) {
        return -2;
    }

    // copy constructor
    TE::Image copy(image);
    if (image.getWidth() != copy.getWidth()) {
        return -1;
    }

    if (image.getHeight() != copy.getHeight()) {
        return -1;
    }

    if (std::memcmp(image.getData(), copy.getData(), SIZE) != 0) {
        return -1;
    }

    return 0;
}