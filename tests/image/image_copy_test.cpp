#include <cstring>
#include <ctime>

#include <image.hpp>

const int WIDTH = 10;
const int HEIGHT = 15;
constexpr int SIZE = WIDTH * HEIGHT;

char DATA[SIZE];

bool initData(char* data, const int size) {
    if (!data) {
        return false;
    }

    for (int i = 0; i < size; ++i) {
        data[i] = rand() % 255;
    }

    return true;
}

int main() {
    srand(time(nullptr));
    
    if (!initData(DATA, SIZE)) {
        return -2;
    }

    //test copy operator
    TE::Image image(WIDTH, HEIGHT, DATA);
    TE::Image copy = image;

    if (image.getWidth() != copy.getWidth()) {
        return -1;
    }

    if (image.getHeight() != copy.getHeight()) {
        return -1;
    }

    if (std::memcmp(image.getData(), copy.getData(), SIZE) != 0) {
        return -1;
    }

    return 0;
}