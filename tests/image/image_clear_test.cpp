#include <image.hpp>

#include <cstring>

const int WIDTH = 10;
const int HEIGHT = 10;
constexpr int SIZE = WIDTH * HEIGHT;
char DATA[SIZE];

void init(bool empty = false) {
    if (empty) {
        std::memset(DATA, '0', SIZE);
    }

    for (int i = 0; i < SIZE; ++i) {
        DATA[i] = i + 1;
    }
}



int main() {
    init();
    
    TE::Image image(WIDTH, HEIGHT, DATA);
    image.clear();

    init(true);

    return std::memcmp(image.getData(), DATA, SIZE) == 0;
}