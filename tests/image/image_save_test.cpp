#include <cstring>
#include <fstream>

#include <image.hpp>

const std::string path = "./../../../resources/test_save_image.imgte";

const int WIDTH = 8;
const int HEIGHT = 8;
constexpr int SIZE = WIDTH * HEIGHT;
const char DATA[] = "*********##**##************##******##****######**######*********";

char* load() {
    std::ifstream reader;
    reader.open(path);

    if (!reader.is_open()) {
        return nullptr;
    }

    std::string buffer;
    int width = 0;
    int height = 0;

    try {
        std::getline(reader, buffer);
        width = std::stoi(buffer);

        std::getline(reader, buffer);
        height = std::stoi(buffer);
    } catch (const std::exception& e) {
        // TODO: log
        reader.close();
        return nullptr;
    }

    const int size = width * height;
    char* data = new char[size + 1];
    data[size] = '\0';

    for (int i = 0; i < size; i += width) {
        std::getline(reader, buffer);
        std::memcpy(data + i, buffer.data(), width);
    }

    reader.close();
    return data;
}

int main() {
    TE::Image image(WIDTH, HEIGHT, DATA);
    if (!image.saveToFile(path)) {
        return -1;
    }

    char* loaded = load();
    if (!loaded) {
        return -1;
    }

    int res = std::memcmp(DATA, loaded, SIZE);

    delete[] loaded;

    if (res != 0) {
        return -1;
    }

    return 0;
};