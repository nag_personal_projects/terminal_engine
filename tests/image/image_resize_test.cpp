#include <image.hpp>

#include <cstring>

const int SRC_WIDTH = 4;
const int SRC_HEIGHT = 3;
constexpr int SRC_SIZE = SRC_WIDTH * SRC_HEIGHT;

const char SRC_DATA[] = "*2*22*2**2*2";

const int DEST_WIDTH_BIG = 8;
const int DEST_HEIGHT_BIG = 6;
constexpr int DEST_SIZE_BIG = DEST_WIDTH_BIG * DEST_HEIGHT_BIG;

const char DEST_DATA_BIG[] = "*2*200002*2*0000*2*20000000000000000000000000000";

const int DEST_WIDTH_SMALL = 2;
const int DEST_HEIGHT_SMALL = 2;
constexpr int DEST_SIZE_SMALL = DEST_WIDTH_SMALL * DEST_HEIGHT_SMALL;

const char DEST_DATA_SMALL[] = "*22*";

int main() {
    TE::Image image(SRC_WIDTH, SRC_HEIGHT, SRC_DATA);

    image.resize(DEST_WIDTH_BIG, DEST_HEIGHT_BIG);
    if (std::memcmp(image.getData(), DEST_DATA_BIG, DEST_SIZE_BIG) != 0) {
        return -1;
    }

    image.resize(DEST_WIDTH_SMALL, DEST_HEIGHT_SMALL);
    if (std::memcmp(image.getData(), DEST_DATA_SMALL, DEST_SIZE_SMALL) != 0) {
        return -1;
    }

    return 0;
}