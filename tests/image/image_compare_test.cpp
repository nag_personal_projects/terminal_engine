#include <image.hpp>

const int WIDTH = 4;
const int HEIGHT = 3;
const int SIZE = 12;

const char DATA_TRUE[] = "*0*0*0*0*0*0";
const char DATA_FALSE[] = "*0*0*0***0*0";

int main() {
    TE::Image image(WIDTH, HEIGHT, DATA_TRUE);

    //the same data and size
    TE::Image image_DTST(WIDTH, HEIGHT, DATA_TRUE);
    if (image != image_DTST) {
        return -1;
    }

    //different data and the same size
    TE::Image image_DFST(WIDTH, HEIGHT, DATA_FALSE);
    if (image == image_DFST) {
        return -1;
    }

    //the same data and different width
    TE::Image image_DTWF(WIDTH - 1, HEIGHT, DATA_TRUE);
    if (image == image_DTWF) {
        return -1;
    }

    // the same data and different height
    TE::Image image_DTHF(WIDTH, HEIGHT - 1, DATA_TRUE);
    if (image == image_DTHF) {
        return -1;
    }

    // the same data and swapped size
    TE::Image image_DTSS(HEIGHT, WIDTH, DATA_TRUE);
    if (image == image_DTSS) {
        return -1;
    }

    return 0;
}