#include <animation_system/animation.hpp>

#include <string>

enum ExitCodes {
    Success = 0,
    BadName = -1,
    BadDelay = -2,
    BadLooped = -3,
    BadInterruptable = -4,
    BadReversed = -5,
    BadDirType = -6,
    BadNumFrames = -7,
    BadCurrFrame = -8,
    BadPlaying = -9
};

const std::string NAME = "TestAnimation";
const std::string NEW_NAME = "NewTestAnimationName";

const int DELAY = 10;
const int NEW_DELAY = 10;

int main() {
    TE::Animation anim(NAME, DELAY);

    std::string name = anim.getName();
    if (name != NAME) {
        return ExitCodes::BadName;
    }

    int delay = anim.getDelay();
    if (delay != DELAY) {
        return ExitCodes::BadDelay;
    }

    anim.setName(NEW_NAME);
    name = anim.getName();
    if (name != NEW_NAME) {
        return ExitCodes::BadName;
    }

    anim.setDelay(NEW_DELAY);
    delay = anim.getDelay();
    if (delay != NEW_DELAY) {
        return ExitCodes::BadDelay;
    }

    anim.setLooped(true);
    if (!anim.isLooped()) {
        return ExitCodes::BadLooped;
    }

    anim.setLooped(false);
    if (anim.isLooped()) {
        return ExitCodes::BadLooped;
    }

    anim.setInterruptable(true);
    if (!anim.isInterruptable()) {
        return ExitCodes::BadInterruptable;
    }

    anim.setInterruptable(false);
    if (anim.isInterruptable()) {
        return ExitCodes::BadInterruptable;
    }

    anim.setReversed(true);
    if (!anim.isReversed()) {
        return ExitCodes::BadReversed;
    }

    anim.setReversed(false);
    if (anim.isReversed()) {
        return ExitCodes::BadReversed;
    }

    anim.setDirectionType(TE::AnimDirection::FORWARD);
    if (anim.getDirectionType() != TE::AnimDirection::FORWARD) {
        return ExitCodes::BadDirType;
    }

    anim.setDirectionType(TE::AnimDirection::BACKWARD);
    if (anim.getDirectionType() != TE::AnimDirection::BACKWARD) {
        return ExitCodes::BadDirType;
    }

    if (anim.getFramesNum() != 0) {
        return ExitCodes::BadNumFrames;
    }

    if (anim.getCurrentFrame() != nullptr) {
        return ExitCodes::BadCurrFrame;
    }

    if (anim.isPlaying() == true) {
        return ExitCodes::BadPlaying;
    }

    anim.play();
    // there are no frames to play
    if (anim.isPlaying() == true) {
        return ExitCodes::BadPlaying;
    }

    return ExitCodes::Success;
}