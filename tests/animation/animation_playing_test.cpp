#include <animation_system/animation.hpp>

#include <cstring>

enum ExitCodes {
    Success = 0,
    BadCurrFrame = -1,
    BadFrame = -2,
    BadNumFrames = -3,
    BadPlaying = -4
};

const std::string NAME = "TestAnimation";

const int WIDTH = 8;
const int HEIGHT = 8;
constexpr int SIZE = WIDTH * HEIGHT;

const char FRAME_0[] = "***##*****#**#***#****#*#******##******#*#****#***#**#*****##***";
const char FRAME_1[] = "########***##******##******##******##******##******##***########";
const char FRAME_2[] = "########**#**#****#**#****#**#****#**#****#**#****#**#**########";

namespace TE {

    // Fake image object
    struct Image {

        char* m_data;

        Image(const char* data) :
            m_data(new char[SIZE + 1])
        {
            std::memcpy(m_data, data, SIZE);
            m_data[SIZE] = '\0';
        }

        ~Image() {
            delete[] m_data;
        }

    };

    typedef std::shared_ptr<Image> ImagePtr;

};


int main() {
    // init frames
    auto frame_0 = std::make_shared<TE::Image>(FRAME_0);
    auto frame_1 = std::make_shared<TE::Image>(FRAME_1);
    auto frame_2 = std::make_shared<TE::Image>(FRAME_2);
    
    // init animation
    TE::Animation anim(NAME);

    // append frames
    anim.appendFrame(frame_0);
    anim.appendFrame(frame_1);
    anim.appendFrame(frame_2);

    // the frames number must be equal 3
    if (anim.getFramesNum() != 3) {
        return ExitCodes::BadNumFrames;
    }

    if (anim.getFrame(0) != frame_0 ||
        anim.getFrame(1) != frame_1 ||
        anim.getFrame(2) != frame_2)
    {
        return ExitCodes::BadFrame;
    }

    // first appended frame become the current frame
    auto currentFrame = anim.getCurrentFrame();
    if (currentFrame == nullptr || currentFrame != frame_0) {
        return ExitCodes::BadCurrFrame;
    }

    anim.play();
    if (anim.isPlaying() == false) {
        return ExitCodes::BadPlaying;
    }

    // to the last frame
    anim.next();
    anim.next();
    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_2) {
        return ExitCodes::BadCurrFrame;
    }

    // if animation is not looped it stops at the last frame
    anim.next();
    if (anim.isPlaying() == true) {
        return ExitCodes::BadPlaying;
    }

    // check the current animation frame is not changed
    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_2) {
        return ExitCodes::BadCurrFrame;
    }

    anim.play();
    if (anim.isPlaying() != true) {
        return ExitCodes::BadPlaying;
    }

    // trying to get a frame after the last frame
    anim.next();

    // check the current animation frame is not changed
    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_2) {
        return ExitCodes::BadCurrFrame;
    }

    if (anim.isPlaying() == true) {
        return ExitCodes::BadPlaying;
    }

    anim.play();

    // to the first frame
    anim.prev();
    anim.prev();
    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_0) {
        return ExitCodes::BadCurrFrame;
    }

    anim.prev();
    // if animation is not looped it stops at the first frame
    if (anim.isPlaying() == true) {
        return ExitCodes::BadPlaying;
    }

    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_0) {
        return ExitCodes::BadCurrFrame;
    }

    anim.play();
    if (anim.isPlaying() != true) {
        return ExitCodes::BadPlaying;
    }

    // trying to get the -1'th frame
    anim.prev();
    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_0) {
        return ExitCodes::BadCurrFrame;
    }

    if (anim.isPlaying() == true) {
        return ExitCodes::BadPlaying;
    }

    anim.setLooped(true);

    anim.play();
    anim.next();
    anim.next();
    anim.next();

    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_0) {
        return ExitCodes::BadCurrFrame;
    }

    anim.prev();
    anim.prev();
    anim.prev();

    currentFrame = anim.getCurrentFrame();
    if (currentFrame != frame_0) {
        return ExitCodes::BadCurrFrame;
    }

    return ExitCodes::Success;
}