#include "test_object.hpp"

#include <string>

static const int TEST_WIDTH = 4;
static const int TEST_HEIGHT = 5;
static const int TEST_SIZE = 6;

static const float TEST_X = 7;
static const float TEST_Y = 8;
static const float TEST_POS = 9;

static const bool TEST_BOOL_TRUE = true;
static const bool TEST_BOOL_FALSE = false;

static const std::string TEST_EMPTY_STRING = "";
static const std::string TEST_DEFAULT_TAG = "TAG";

enum EXIT_CODE {
    SUCCESS = 0,
    WIDTH_FAIL = 1,
    HEIGHT_FAIL = 2,
    SIZE_FAIL = 3,
    X_FAIL = 4,
    Y_FAIL = 5,
    POS_FAIL = 6,
    VISIBLE_FAIL = 7,
    DESTROY_FAIL = 8,
    APPEND_TAG_FAIL = 9,
    REMOVE_TAG_FAIL = 10,
    HAS_TAG_FAIL = 11,
    EMPTY_TAG_FAIL = 12
};

int main() {
    auto object = std::make_shared<TestObject>();

    object->setWidth(TEST_WIDTH);
    if (TEST_WIDTH != object->getWidth()) {
        return WIDTH_FAIL;
    }

    object->setHeight(TEST_HEIGHT);
    if (TEST_HEIGHT != object->getHeight()) {
        return HEIGHT_FAIL;
    }

    object->setSize(TEST_SIZE, TEST_SIZE);
    if (TEST_SIZE != object->getWidth() || TEST_SIZE != object->getHeight()) {
        return SIZE_FAIL;
    }

    object->setPosX(TEST_X);
    if (TEST_X != object->getPosX()) {
        return X_FAIL;
    }

    object->setPosY(TEST_Y);
    if (TEST_Y != object->getPosY()) {
        return Y_FAIL;
    }

    object->setPosition(TEST_POS, TEST_POS);
    if (TEST_POS != object->getPosX() || TEST_POS != object->getPosY()) {
        return POS_FAIL;
    }

    object->setVisible(TEST_BOOL_TRUE);
    if (TEST_BOOL_TRUE != object->isVisible()) {
        return VISIBLE_FAIL;
    }

    object->setVisible(TEST_BOOL_FALSE);
    if (TEST_BOOL_FALSE != object->isVisible()) {
        return VISIBLE_FAIL;
    }

    object->destroy();
    if (TEST_BOOL_TRUE != object->isDestroyed()) {
        return DESTROY_FAIL;
    }

    object->appendTag(TEST_EMPTY_STRING);
    if (object->getTagsNum() != 0) {
        return EMPTY_TAG_FAIL;
    }

    object->appendTag(TEST_DEFAULT_TAG);
    if (object->getTagsNum() != 1) {
        return APPEND_TAG_FAIL;
    }

    if (!object->hasTag(TEST_DEFAULT_TAG)) {
        return HAS_TAG_FAIL;
    }

    object->removeTag(TEST_DEFAULT_TAG);
    if (object->getTagsNum() != 0) {
        return REMOVE_TAG_FAIL;
    }

    return SUCCESS;
}