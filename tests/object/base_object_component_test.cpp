#include "test_object.hpp"
#include "test_component.hpp"

int main() {
    ObjectPtr object = std::make_shared<TestObject>();

    // Do not create components manually please! 
    // Use 'appendComponent' method of TE::Object to create a component.
    //TE::ComponentPtr _component = std::make_shared<TestComponent>(object.get());

    // Trying to get not existing component
    TE::ComponentPtr component = object->getComponent<TestComponent>();
    if (component != nullptr) {
        return -1;
    }
    // Trying to append component by type
    component = object->appendComponent<TestComponent>();
    if (object->getComponentsNum() != 1 || !component) {
        return -1;
    }

    // Trying to get a component by type
    TE::ComponentPtr c = object->getComponent<TestComponent>();
    if (component != c) {
        return -1;
    }

    // Trying to get a component by index
    component = object->getComponent(0);
    if (component != c) {
        return -1;
    }

    // Trying to append component that the object already has
    c = object->appendComponent<TestComponent>();
    if (c != component) {
        return -1;
    }

    // Trying to remove an existing component
    object->removeComponent<TestComponent>();
    if (object->getComponentsNum() != 0) {
        return -1;
    }

    return 0;
}