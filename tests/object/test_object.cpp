#include "test_object.hpp"

TestObject::TestObject() : Object(0, 0 ,0 ,0) {}

void TestObject::onDestroy() {}

void TestObject::onUpdate(clock_t FPS) {}
    
void TestObject::onDraw(const CanvasPtr& canvas) {}

void TestObject::onIntersects(const ObjectPtr& object) {}