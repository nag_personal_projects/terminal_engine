#include <object.hpp>

class Canvas;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;

typedef std::shared_ptr<TE::Object> ObjectPtr;

class TestObject : public TE::Object {

public:

    TestObject();
    ~TestObject() override = default;

protected:

    void onDestroy() override;

    void onUpdate(clock_t FPS) override;
        
    void onDraw(const CanvasPtr& canvas) override;

    void onIntersects(const ObjectPtr& object) override;

};