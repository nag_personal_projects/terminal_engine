#ifndef TEST_COMPONENT_HPP
#define TEST_COMPONENT_HPP

#include <component.hpp>

class TestComponent : public TE::Component {

    public:

        TestComponent(TE::Object* parent);
        ~TestComponent() override = default;

    private:

        void update(clock_t FPS) override;

};

#endif //!TEST_COMPONENT_HPP