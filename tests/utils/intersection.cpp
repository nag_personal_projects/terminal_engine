#include <utils.hpp>

enum {
    SUCCESS = 0,
    IT_FAIL = 1,
    IF_FAIL = 2,
    INSIDE_FAIL = 3,
    EQUAL_FAIL = 4
};

struct Rect {
    int x;
    int y;
    int width;
    int height;
};

using namespace TE::Utils;

int main() {
    const Rect m_rect = {1, 1, 3, 3};

    const Rect it_rect = {3, 3, 3, 3};
    const bool i_true = TE::Utils::isIntersected(m_rect.x, m_rect.y, m_rect.width, m_rect.height,
                                                 it_rect.x, it_rect.y, it_rect.width, it_rect.height);

    if (!i_true) {
        return IT_FAIL;
    }

    const Rect if_rect = {5, 5, 3, 3};
    const bool i_false = TE::Utils::isIntersected(m_rect.x, m_rect.y, m_rect.width, m_rect.height,
                                                  if_rect.x, if_rect.y, if_rect.width, if_rect.height);

    if (i_false) {
        return IF_FAIL;
    }

    const Rect inside_rect = {2, 2, 1, 1};
    const bool inside = TE::Utils::isIntersected(m_rect.x, m_rect.y, m_rect.width, m_rect.height,
                                                 inside_rect.x, inside_rect.y, inside_rect.width, inside_rect.height);

    if (!inside) {
        return INSIDE_FAIL;
    }

    bool equal = TE::Utils::isIntersected(m_rect.x, m_rect.y, m_rect.width, m_rect.height,
                                          m_rect.x, m_rect.y, m_rect.width, m_rect.height);

    if (!equal) {
        return EQUAL_FAIL;
    }

    return SUCCESS;
}