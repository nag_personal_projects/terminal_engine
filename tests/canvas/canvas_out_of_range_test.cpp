#include <memory>

#include <canvas.hpp>

typedef std::shared_ptr<TE::Canvas> CanvasPtr;

static const int SIZE = 10;

int main() {
    CanvasPtr canvas = std::make_shared<TE::Canvas>(SIZE, SIZE);

    if (canvas->at(-1, -1) != canvas->at(0, 0)) {
        return -1;
    }

    if (canvas->at(2, 25) != canvas->at(2, SIZE - 1)) {
        return -1;
    }

    if (canvas->at(15, 5) != canvas->at(SIZE - 1, 5)) {
        return -1;
    }

    if (canvas->at(36, 236) != canvas->at(SIZE - 1, SIZE - 1)) {
        return -1;
    }

    return 0;
}