#include "event_system.hpp"
#include "object.hpp"

#include <algorithm>

namespace TE {

    void EventSystem::appendSubscriber(Object* sub) {
        if (sub) {
            auto it = std::find(m_subs.begin(), m_subs.end(), sub);
            if (it == m_subs.end()) {
                m_subs.push_back(sub);
            }
        }

    }

    void EventSystem::removeSubscriber(size_t index) {
        if (index < m_subs.size()) {
            auto pos = m_subs.begin() + static_cast<long>(index);
            m_subs.erase(pos);
        }
    }

    void EventSystem::removeSubscriber(Object* sub) {
        if (sub) {
            auto it = std::find(m_subs.begin(), m_subs.end(), sub);
            if (it != m_subs.end()) {
                m_subs.erase(it);
            }
        }
    }

    size_t EventSystem::getNumSubscribers() const {
        return m_subs.size();
    }

    void EventSystem::send(const EventPtr& event) {
        for (Object* object : m_subs) {
            object->onEvent(event);
        }
    }

}