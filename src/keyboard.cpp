#include "keyboard.hpp"

#include <iostream>

namespace TE {

    int Keyboard::m_key = -1;
    KeyState Keyboard::m_keyState = KeyState::Undefined;
    bool Keyboard::m_isStarted = false;

    Keyboard::~Keyboard() {
        m_isStarted = false;
        if (m_listener.joinable()) {
            m_listener.join();
        }
    }

    void Keyboard::start() {
        m_display = XOpenDisplay(NULL);
        assert(m_display);

        m_window = XDefaultRootWindow(m_display);
        XGrabKeyboard(m_display, m_window, True, GrabModeAsync, GrabModeAsync, CurrentTime);

        m_isStarted = true;
        m_listener = std::thread(&Keyboard::loop, this);
    }

    void Keyboard::stop() {
        m_isStarted = false;
        if (m_listener.joinable()) {
            m_listener.join();
        }

        XUngrabKeyboard(m_display, CurrentTime);
        XCloseDisplay(m_display);
        m_display = nullptr;
    }

    void Keyboard::loop() {
        XEvent event;

        while (m_isStarted) {
            XNextEvent(m_display, &event);

            m_key = XkbKeycodeToKeysym(m_display, event.xkey.keycode, 0, event.xkey.state & ShiftMask ? 1 : 0);
            m_keyState = static_cast<KeyState>(event.type);
        }
    }

    bool Keyboard::isKeyPressed(int key) {
        return m_key == key && m_keyState == KeyState::Pressed;
    }

    bool Keyboard::isKeyReleased(int key) {
        return m_key == key && m_keyState == KeyState::Released;
    }

}