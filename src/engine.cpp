#include "engine.hpp"
#include "scene.hpp"
#include "keyboard.hpp"

#include <iostream>
#include <cassert>
#include <chrono>

using namespace std::chrono;

namespace TE {

	Engine::Engine(const std::string& appName,
				   const ScenePtr& scene) :
		m_appName(appName),
		m_scene(scene),
		m_keyListener(std::make_shared<Keyboard>())
	{
		m_isRunning = false;

		m_fps = 0;
		m_fpsLimit = 0;
		m_isFPSLimited = false;
	}

	Engine::~Engine() {
		m_isRunning = false;
		if (m_loopThread.joinable()) {
			m_loopThread.join();
		}
	}

	void Engine::run() {
		if (m_isRunning) {
			return;
		}

		std::cout << "engine is running" << std::endl;
		m_isRunning = true;

		m_keyListener->start();
		m_loopThread = std::thread(&Engine::loop, this);
	}

	void Engine::stop() {
		if (!m_isRunning) {
			return;
		}

		m_keyListener->stop();

		m_isRunning = false;
		m_loopThread.join();

		std::cout << "engine stopped" << std::endl;
	}

	void Engine::setLimitFPS(int limit) {
		assert(limit > 0);
		m_fpsLimit = limit;
	}

	void Engine::limitFPS(bool value) {
		m_isFPSLimited = value;
	}

	bool Engine::isFPSLimited() const {
		return m_isFPSLimited;
	}

	clock_t Engine::getFPS() const {
		return m_fps;
	}

	bool Engine::isRunning() const {
		return m_isRunning;
	}

	void Engine::setAppName(const std::string& appName) {
		m_appName = appName;
	}

	const std::string& Engine::getAppName() const {
		return m_appName;
	}

	void Engine::setScene(const ScenePtr& scene) {
		if (!scene || scene == m_scene) {
			return;
		}

		m_scene = scene;
	}

	const ScenePtr& Engine::getScene() const {
		return m_scene;
	}

	void Engine::loop() {
		if (!m_scene) {
			printf("E: %s -> scene is nullptr\n", __FUNCTION__);
			m_isRunning = false;
			return;
		}

		time_point<system_clock, nanoseconds> start;
		uint64_t delta = 0;

		while (m_isRunning) {
			start = system_clock::now();

			m_scene->update(m_fps);

			if (m_isFPSLimited && m_fpsLimit != 0) {
				std::this_thread::sleep_for(milliseconds((1000 / m_fpsLimit)));
			}

			delta = duration_cast<nanoseconds>(system_clock::now() - start).count();
			if (delta > 0) {
				m_fps = duration_cast<nanoseconds>(milliseconds(1000)).count() / delta;
			}

			std::cout << "\033]0;" << m_appName << " : FPS - " << m_fps << "\007";
		}
	}

}
