#include "timer.hpp"

#include <chrono>
#include <cmath>

namespace TE {

    Timer::Timer(uint64_t time, const std::function<void()>& callback, bool loop, bool start) :
        m_callback(callback)
    {
        m_time = time;
        m_loop = loop;
        m_currentTime = 0;

        if (start) {
            this->start();
        }
    }

    Timer::~Timer() {
        m_isStarted = false;

        if (m_timerThread.joinable()) {
            m_timerThread.join();
        }
    }

    void Timer::start() {
        if (m_isStarted) {
            return;
        }

        m_isStarted = true;

        m_timerThread = std::thread(&Timer::tick, this);
    }

    void Timer::stop() {
        if (m_isStarted) {
            m_isStarted = false;
            m_timerThread.join();
        }
    }

    void Timer::tick() {
        while (m_isStarted) {
            m_currentTime++;

            if (m_currentTime == m_time) {
                m_callback();

                if (m_loop) {
                    m_currentTime = 0;
                } else {
                     m_isStarted = false;
                }
            }

            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }

};