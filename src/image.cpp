#include "image.hpp"

#include <cstring>
#include <stdexcept>

#include <fstream>

namespace TE {

    Image::Image() :
        m_data(nullptr)
    {
        m_width = 0;
        m_height = 0;
    }
    
    Image::Image(int width, int height, const char* data)  {
        m_width = width < 0 ? 0 : width;
        m_height = height < 0 ? 0 : height;

        const int size = width * height;
        m_data = new char[size + 1];
        m_data[size] = '\0';

        if (data == nullptr) {
            std::memset(m_data, '0', size);
        } else {
            std::memcpy(m_data, data, size);
        }
    }
    
    Image::Image(const Image& image) {
        m_width = image.m_width;
        m_height = image.m_height;

        const int size = m_width * m_height;
        m_data = new char[size + 1];
        std::memcpy(m_data, image.m_data, size);
        m_data[size] = '\0';
    }
    
    Image::Image(Image&& image) {
        m_width = image.m_width;
        m_height = image.m_height;
        m_data = image.m_data;

        image.m_width = 0;
        image.m_height = 0;
        image.m_data = nullptr;
    }
    
    Image::~Image() {
        delete[] m_data;
    }

    void Image::setWidth(int width) {
        if (m_width == width) {
            return;
        }

        const int oldWidth = m_width;
        const int oldSize = m_width * m_height;

        m_width = width;
        const int size = m_width * m_height;

        char* data = m_data;
        m_data = new char[size + 1];
        m_data[size] = '\0';

        std::memset(m_data, '0', size);

        char* buffer = new char[oldWidth];
        for (int i = 0, j = 0; i < oldSize; i += oldWidth, ++j) {
            std::memcpy(buffer, data + i, oldWidth);
            std::memmove(m_data + (j * m_width), buffer, oldWidth);
        }

        delete[] buffer;
        delete[] data;
    }
    
    int Image::getWidth() const {
        return m_width;
    }

    void Image::setHeight(int height) {
        if (m_height == height) {
            return;
        }

        const int oHeight = m_height;
        m_height = height;

        const int oldSize = m_width * oHeight;
        const int size = m_width * m_height;
        const int deltaSize = oldSize - size;

        char* data = m_data;
        m_data = new char[size + 1];
        m_data[size] = '\0';

        std::memset(m_data, '0', size);
        std::memcpy(m_data, data, oldSize);

        delete[] data;
    }
    
    int Image::getHeight() const {
        return m_height;
    }

    void Image::resize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }

    const char& Image::at(int x, int y) const {
        const int index = y * m_width + x;
        if (index < 0 || index >= m_width * m_height) {
            throw (std::out_of_range("bad index"));
        }

        return m_data[index];
    }

    char& Image::at(int x, int y) {
        const int index = y * m_width + x;
        if (index < 0 || index >= m_width * m_height) {
            throw (std::out_of_range("bad index"));
        }

        return m_data[index];
    }

    const char* Image::getData() const {
        return m_data;
    }
    
    char* Image::getData() {
        return m_data;
    }

    void Image::clear() {
        std::memset(m_data, '0', m_width * m_height);
    }

    bool Image::compare(const Image& image) const {
        if (m_width != image.m_width ||
            m_height != image.m_height)
        {
            return false;
        }

        return !std::memcmp(m_data, image.m_data, m_width * m_height);
    }

    bool Image::saveToFile(const std::string& path) {
        std::ofstream writer;
        writer.open(path, std::ios::out);

        if (!writer.is_open()) {
            return false;
        }

        writer << std::to_string(m_width) << std::endl;
        writer << std::to_string(m_height) << std::endl;

        for (int i = 0; i < m_height; ++i) {
            writer << std::string(m_data + (i * m_width), m_width) << std::endl;
        }

        writer.close();
        return true;
    }

    bool Image::loadFromFile(const std::string& path) {
        std::ifstream reader;
        reader.open(path, std::ios::in);

        if (!reader.is_open()) {
            return false;
        }

        std::string lineBuffer;

        int width = 0;
        int height = 0;

        try {
            std::getline(reader, lineBuffer);
            width = std::stoi(lineBuffer);

            std::getline(reader, lineBuffer);
            height = std::stoi(lineBuffer);
        } catch (const std::exception& e) {
            // TODO: log
            reader.close();
            return false;
        }

        const int size = width * height;
        char* data = new char[size + 1];
        data[size] = '\0';

        for (int i = 0; i < size; i += width) {
            std::getline(reader, lineBuffer);
            std::memcpy(data + i, lineBuffer.data(), width);
        }

        if (m_data) {
            delete[] m_data;
        }

        m_width = width;
        m_height = height;
        m_data = data;

        reader.close();
        return true;
    }

    ImagePtr Image::createFromFile(const std::string& path) {
        auto image = std::make_shared<Image>();
        if (image->loadFromFile(path)) {
            return image;
        }

        return nullptr;
    }

    Image& Image::operator=(const Image& image) {
        delete[] m_data;

        m_width = image.m_width;
        m_height = image.m_height;

        const int size = m_width * m_height;
        m_data = new char[size + 1];
        std::memcpy(m_data, image.m_data, size);
        m_data[size] = '\0';

        return *this;
    }
    
    Image& Image::operator=(Image&& image) {
        delete m_data;

        m_width = image.m_width;
        m_height = image.m_height;
        m_data = image.m_data;

        image.m_width = 0;
        image.m_height = 0;
        image.m_data = nullptr;

        return *this;
    }

    bool Image::operator==(const Image& image) const {
        return compare(image);
    }

    bool Image::operator!=(const Image& image) const {
        return !compare(image);
    }

}