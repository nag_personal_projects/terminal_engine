#include "animation_system/animation.hpp"

#include <algorithm>

namespace TE {

    Animation::Animation(const std::string& name, int delay) :
        m_currentFrame(nullptr)
    {
        static int id = 0;
        m_name = name.empty() ? "anim_" + std::to_string(++id) : name;

        m_isInterruptable = false;
        m_isReversed = false;
        m_isPlaying = false;
        m_isLooped = false;

        m_currentIndex = 0;
        m_direction = AnimDirection::FORWARD;

        m_time = 0;
        m_delay = delay;
    }
    
    Animation::Animation(const Animation& animation) {
        m_name = animation.m_name;

        m_isInterruptable = animation.m_isInterruptable;
        m_isReversed = animation.m_isReversed;
        m_isPlaying = false;
        m_isLooped = animation.m_isLooped;
        
        m_currentIndex = 0;
        m_delay = animation.m_delay;
        m_time = 0;

        m_direction = animation.m_direction;

        m_frames = animation.m_frames;
        m_currentFrame = m_frames.empty() ? nullptr : *m_frames.begin();
    }
    
    Animation::Animation(Animation&& animation) {
        m_name = animation.m_name;

        m_isInterruptable = animation.m_isInterruptable;
        m_isReversed = animation.m_isReversed;
        m_isPlaying = false;
        m_isLooped = animation.m_isLooped;
        
        m_currentIndex = 0;
        m_delay = animation.m_delay;
        m_time = 0;

        m_direction = animation.m_direction;

        m_frames = animation.m_frames;
        m_currentFrame = m_frames.empty() ? nullptr : *m_frames.begin();

        animation.m_name.clear();
        animation.m_isInterruptable = false;
        animation.m_isReversed = false;
        animation.m_isPlaying = false;
        animation.m_isLooped = false;
        
        animation.m_currentIndex = 0;
        animation.m_delay = 0;
        animation.m_time = 0;

        animation.m_frames.clear();
        animation.m_currentFrame = nullptr;
    }
    
    Animation::~Animation() {
        m_frames.clear();
        m_currentFrame = nullptr;
    }

    void Animation::setName(const std::string& name) {
        if (!name.empty()) {
            m_name = name;
        }
    }
    
    const std::string& Animation::getName() const {
        return m_name;
    }

    void Animation::setLooped(bool looped) {
        m_isLooped = looped;
    }
    
    bool Animation::isLooped() const {
        return m_isLooped;
    }

    void Animation::setReversed(bool reversed) {
        m_isReversed = reversed;
    }
    
    bool Animation::isReversed() const {
        return m_isReversed;
    }

    void Animation::setInterruptable(bool interruptable) {
        m_isInterruptable = interruptable;
    }
    
    bool Animation::isInterruptable() const {
        return m_isInterruptable;
    }

    void Animation::setDirectionType(AnimDirection direction) {
        m_direction = direction;
    }
    
    AnimDirection Animation::getDirectionType() const {
        return m_direction;
    }

    void Animation::setDelay(int delay) {
        m_delay = delay;
    }

    int Animation::getDelay() const {
        return m_delay;
    }

    bool Animation::isPlaying() const {
        return m_isPlaying;
    }

    void Animation::appendFrame(const ImagePtr& frame) {
        if (!frame) {
            return;
        }

        auto it = std::find(m_frames.begin(), m_frames.end(), frame);
        if (it == m_frames.end()) {
            m_frames.push_back(frame);

            if (!m_currentFrame) {
                m_currentFrame = frame;
            }
        }

    }

    void Animation::removeFrame(const ImagePtr& frame) {
        auto it = std::find(m_frames.begin(), m_frames.end(), frame);
        if (it != m_frames.end()) {
            size_t index = std::distance(m_frames.begin(), it);
            removeFrame(index);
        }
    }

    void Animation::removeFrame(size_t index) {
        if (index >= m_frames.size()) {
            return;
        }

        if (m_currentIndex == index) {
            if (m_frames.size() == 1) {
                m_currentFrame = nullptr;
                stop();
            } else if (index = m_frames.size() - 1) {
                m_currentIndex = 0;
                m_currentFrame = m_frames[0];
            } else {
                m_currentIndex = index + 1;
                m_currentFrame = m_frames[index + 1];
            }
        }

        auto it = m_frames.begin() + static_cast<unsigned long>(index);
        m_frames.erase(it);
    }

    size_t Animation::getFramesNum() const {
        return m_frames.size();
    }

    ImagePtr Animation::getFrame(size_t index) const {
        return index >= m_frames.size() ? nullptr : m_frames[index];
    }

    const ImagePtr& Animation::getCurrentFrame() const {
        return m_currentFrame;
    }

    void Animation::play() {
        if (m_isPlaying || m_frames.empty()) {
            return;
        }

        m_isPlaying = true;
    }

    void Animation::stop() {
        m_isPlaying = false;
    }

    void Animation::next() {
        if (!m_isPlaying) {
            return;
        }

        m_currentIndex += m_currentIndex == - 1 ? 2 : 1;

        if (m_currentIndex >= static_cast<int>(m_frames.size())) {
            if (m_isLooped == false) {
                m_currentIndex = static_cast<int>(m_frames.size());
                stop();
                return;
            }

            m_currentIndex = 0;
        }

        m_currentFrame = m_frames[static_cast<size_t>(m_currentIndex)];
    }

    void Animation::prev() {
        if (!m_isPlaying) {
            return;
        }

        m_currentIndex -= m_currentIndex == static_cast<int>(m_frames.size()) ? 2 : 1;

        if (m_currentIndex <= -1) {
            if (m_isLooped == false) {
                m_currentIndex = -1;
                stop();
                return;
            }

            m_currentIndex = static_cast<int>(m_frames.size() - 1);
        }

        m_currentFrame = m_frames.at(static_cast<size_t>(m_currentIndex));

    }

    void Animation::update() {
        if (!m_isPlaying) {
            return;
        }

        if (m_time > m_delay) {
            m_currentIndex += m_isReversed ? -1 : 1;
            m_time = 0;
        }

        ++m_time;

        if (!m_isReversed) {
            if (m_currentIndex >= static_cast<int>(m_frames.size())) {
                m_currentIndex = 0;
                if (!m_isLooped) {
                    stop();
                }
            }
        } else {
            if (m_currentIndex == -1) {
                m_currentIndex = static_cast<int>(m_frames.size() - 1);
                if (!m_isLooped) {
                    stop();
                }
            }
        }

        m_currentFrame = m_frames[static_cast<size_t>(m_currentIndex)];
    }

    Animation& Animation::operator=(const Animation& animation) {
        m_frames.clear();
        m_currentFrame = nullptr;

        m_name = animation.m_name;

        m_isInterruptable = animation.m_isInterruptable;
        m_isReversed = animation.m_isReversed;
        m_isPlaying = false;
        m_isLooped = animation.m_isLooped;
        
        m_currentIndex = 0;
        m_delay = animation.m_delay;
        m_time = 0;

        m_direction = animation.m_direction;

        m_frames = animation.m_frames;
        m_currentFrame = m_frames.empty() ? nullptr : *m_frames.begin();

        return *this;
    }

    Animation& Animation::operator=(Animation&& animation) {
        m_frames.clear();
        m_currentFrame = nullptr;

        m_name = animation.m_name;

        m_isInterruptable = animation.m_isInterruptable;
        m_isReversed = animation.m_isReversed;
        m_isPlaying = false;
        m_isLooped = animation.m_isLooped;
        
        m_currentIndex = 0;
        m_delay = animation.m_delay;
        m_time = 0;

        m_direction = animation.m_direction;

        m_frames = animation.m_frames;
        m_currentFrame = m_frames.empty() ? nullptr : *m_frames.begin();

        animation.m_name.clear();
        animation.m_isInterruptable = false;
        animation.m_isReversed = false;
        animation.m_isPlaying = false;
        animation.m_isLooped = false;
        
        animation.m_currentIndex = 0;
        animation.m_delay = 0;
        animation.m_time = 0;

        animation.m_frames.clear();
        animation.m_currentFrame = nullptr;

        return *this;
    }

};