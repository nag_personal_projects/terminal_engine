#include "animation_system/animation_component.hpp"
#include "animation_system/animation.hpp"

#include "object.hpp"

namespace TE {

    AnimationComponent::AnimationComponent(Object* parent) :
        Component(parent),
        m_currentAnimation(nullptr) {}

    AnimationComponent::~AnimationComponent() {
        m_animations.clear();
        m_currentAnimation = nullptr;
    }

    void AnimationComponent::appendAnimation(const AnimationPtr& animation) {
        if (!animation) {
            return;
        }

        const std::string& name = animation->getName();

        auto it = m_animations.find(name);
        if (it == m_animations.end()) {
            m_animations[name] = animation;
        }

        if (m_currentAnimation == nullptr) {
            m_currentAnimation = animation;
        }
    }
    
    void AnimationComponent::removeAnimation(const std::string& name) {
        m_animations.erase(name);
        if (m_currentAnimation->getName() == name) {
            m_currentAnimation = nullptr;
        }
    } 

    AnimationPtr AnimationComponent::getAnimation(const std::string& name) {
        auto it = m_animations.find(name);
        return it == m_animations.end() ? nullptr : it->second;
    }

    void AnimationComponent::setCurrentAnimation(const AnimationPtr& animation) {
        m_currentAnimation = animation;
    }
    
    void AnimationComponent::setCurrentAnimation(const std::string& name) {
        auto it = m_animations.find(name);
        m_currentAnimation = it == m_animations.end() ? nullptr : it->second;
    }

    const AnimationPtr& AnimationComponent::getCurrentAnimation() const {
        return m_currentAnimation;
    }

    void AnimationComponent::play() {
        if (m_currentAnimation) {
            m_currentAnimation->play();
        }
    }
    
    void AnimationComponent::stop() {
        if (m_currentAnimation) {
            m_currentAnimation->stop();
        }
    }

    void AnimationComponent::update(clock_t FPS) {
        if (!m_currentAnimation) {
            return;
        }

        m_currentAnimation->update();

        const ImagePtr& frame = m_currentAnimation->getCurrentFrame();
        m_parent->setTexture(frame);
    }

};