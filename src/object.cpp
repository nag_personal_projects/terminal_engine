#include "object.hpp"
#include "canvas.hpp"
#include "event_system.hpp"

#include <algorithm>
#include <cstdio>
#include <string>

namespace TE {

    Object::Object(int width, int height, float x, float y) :
        m_eventSystem(std::make_shared<EventSystem>())
    {
        static size_t id = 0;
        m_id == ++id;

        m_width = width;
        m_height = height;
        m_posX = x;
        m_posY = y;

        m_isDestroyed = false;
        m_isVisible = true;
    }

    size_t Object::getID() const {
        return m_id;
    }

    void Object::setTexture(const ImagePtr& texture) {
        m_texture = texture;
    }

    const ImagePtr& Object::getTexture() const {
        return m_texture;
    }

    void Object::setWidth(int width) {
        m_width = width < 0 ? 0 : width;
    }

    int Object::getWidth() const {
        return m_width;
    }

    void Object::setHeight(int height) {
        m_height = height < 0 ? 0 : height;
    }

    int Object::getHeight() const {
        return m_height;
    }

    void Object::setPosX(float x) {
        m_posX = x < 0 ? 0 : x;
    }

    float Object::getPosX() const {
        return m_posX;
    }

    void Object::setPosY(float y) {
        m_posY = y < 0 ? 0 : y;
    }

    float Object::getPosY() const {
        return m_posY;
    }

    void Object::setSize(int width, int height) {
        m_width = width < 0 ? 0 : width;
        m_height = height < 0 ? 0 : height;     
    }

    void Object::setPosition(float x, float y) {
        m_posX = x < 0 ? 0 : x;
        m_posY = y < 0 ? 0 : y;
    }

    void Object::setVisible(bool visible) {
        m_isVisible = visible;
    }

    bool Object::isVisible() const {
        return m_isVisible;
    }

    bool Object::isDestroyed() const {
        return m_isDestroyed;
    }

    void Object::appendTag(const std::string& tag) {
        if (tag.empty()) {
            return;
        }

        auto it = std::find(m_tags.begin(), m_tags.end(), tag);
        if (it == m_tags.end()) {
            m_tags.push_back(tag);
        }
    }

    void Object::removeTag(const std::string& tag) {
        if (tag.empty()) {
            return;
        }

        auto it = std::find(m_tags.begin(), m_tags.end(), tag);
        if (it != m_tags.end()) {
            m_tags.erase(it);
        }
    }

    void Object::removeTag(size_t index) {
        if (index >= m_tags.size()) {
            return;
        }

        auto it = m_tags.begin() + static_cast<unsigned>(index);
        m_tags.erase(it);
    }

    size_t Object::getTagsNum() const {
        return m_tags.size();
    }

    bool Object::hasTag(const std::string& tag) const {
        if (tag.empty()) {
            return false;
        }

        auto it = std::find(m_tags.begin(), m_tags.end(), tag);
        return it != m_tags.end();
    }

    ComponentPtr Object::getComponent(size_t index) const {
        if (index >= m_components.size()) {
            return nullptr;
        }

        return m_components[index];
    }

    size_t Object::getComponentsNum() const {
        return m_components.size();
    }

    void Object::destroy() {
        if (m_isDestroyed) {
            //TODO: error message
            return;
        }

        m_isDestroyed = true;

        auto event = std::make_shared<Event>(this, this, TE::EventType::Destroyed);
        
        m_eventSystem->send(event);
        m_components.clear();
        
        onDestroy();
    }

    void Object::update(clock_t FPS) {
        if (m_isDestroyed || !m_isVisible) {
            return;
        }

        for (auto component : m_components) {
            component->update(FPS);
        }

        onUpdate(FPS);
    }

    void Object::draw(const CanvasPtr& canvas) {
        if (m_isDestroyed || !m_isVisible) {
            return;
        }

        onDraw(canvas);
    }

    void Object::intersects(const ObjectPtr& object) {
        if (m_isDestroyed || !m_isVisible) {
            return;
        }

        onIntersects(object);
    }

    const EventSystemPtr& Object::getEventSystem() const {
        return m_eventSystem;
    }

}