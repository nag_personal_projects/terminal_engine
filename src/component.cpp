#include "component.hpp"

namespace TE {

    Component::Component(Object* parent) :
        m_parent(parent),
        m_name("") {}

    Object*Component::getParent() const {
        return m_parent;
    }

    const std::string& Component::getName() const {
        return m_name;
    }

};