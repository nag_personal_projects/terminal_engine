#include "utils.hpp"

#include <cmath>
#include <limits>

namespace TE::Utils {

    bool isIntersected(int x1, int y1, int w1, int h1,
                       int x2, int y2, int w2, int h2)
    {
        const int hW1 = w1 / 2;
        const int hW2 = w2 / 2;

        if( x1 + hW1 < x2 - hW2) {
            return false;
        }

        if( x1 - hW1 > x2 + hW2) {
            return false;
        }

        const int hH1 = h1 / 2;
        const int hH2 = h2 / 2;

        if( y1 + hH1 < y2 - hH2) {
            return false;
        }

        if( y1 - hH1 > y2 + hH2) {
            return false;
        }

        return true;
    }

    bool compare(float a, float b) {
        return std::abs(a - b) <= std::numeric_limits<float>::epsilon();
    }

}