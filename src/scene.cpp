#include "scene.hpp"
#include "canvas.hpp"
#include "object.hpp"
#include "utils.hpp"

#include <algorithm>

namespace TE {

    Scene::Scene(int width, int height) :
        m_canvas(std::make_shared<Canvas>(width, height)) {}

    bool Scene::appendObject(const ObjectPtr& object) {
        std::lock_guard<std::recursive_mutex> locker(m_sceneLock);
       

        if (!object) {
            return false;
        }

        auto it = std::find(m_objects.begin(), m_objects.end(), object);
        if (it != m_objects.end()) {
            return false;
        }

        m_objects.push_back(object);
        return true;
    }

    void Scene::removeObject(size_t index) {
        std::lock_guard<std::recursive_mutex> locker(m_sceneLock);

        if (index >= m_objects.size()) {
            return;
        }

        auto it = m_objects.begin() + static_cast<unsigned>(index);
        m_objects.erase(it);
    }

    void Scene::removeObject(const ObjectPtr& object) {
        std::lock_guard<std::recursive_mutex> locker(m_sceneLock);

        if (!object) {
            return;
        }

        auto it = std::find(m_objects.begin(), m_objects.end(), object);
        if (it != m_objects.end()) {
            m_objects.erase(it);
        }
    }

    bool Scene::hasObject(const ObjectPtr& object) const {
        std::lock_guard<std::recursive_mutex> locker(m_sceneLock);

        if (!object) {
            return false;
        }

        auto it = std::find(m_objects.begin(), m_objects.end(), object);
        return it != m_objects.end();
    }

    size_t Scene::getObjectsNum() const {
        return m_objects.size();
    }

    int Scene::getWidth() const {
        return m_canvas->getWidth();
    }

    int Scene::getHeight() const {
        return m_canvas->getHeight();
    }

    void Scene::update(clock_t FPS) {
        std::lock_guard<std::recursive_mutex> locker(m_sceneLock);

        if (!m_objects.empty()) {
            // update all object in the storage and remove the objects marked as destroyed
            for (auto it = m_objects.begin(); it != m_objects.end(); ++it) {
                ObjectPtr object = *it;
                if (object->isDestroyed()) {
                    m_objects.erase(it);
                    --it;
                } else {
                    object->update(FPS);
                }
            }

            // solve objects collision after updating
            for (size_t i = 0; i < m_objects.size() - 1; ++i) {
                ObjectPtr obj_i = m_objects.at(i);

                for (size_t j = i + 1; j < m_objects.size(); ++j) {
                    ObjectPtr obj_j = m_objects.at(j);

                    bool value = Utils::isIntersected(obj_i->getPosX(), obj_i->getPosY(), obj_i->getWidth(), obj_i->getHeight(),
                                                    obj_j->getPosX(), obj_j->getPosY(), obj_j->getWidth(), obj_j->getHeight());

                    if (value) {
                        obj_i->intersects(obj_j);
                        obj_j->intersects(obj_i);
                    }
                }
            }
        }

        // draw updated and solved objects
        draw();
    }

    void Scene::draw() {
        // clean canvas data
        m_canvas->clear();

        // draw objects
        for (const ObjectPtr& object : m_objects) {
            object->draw(m_canvas);
        }

        // display drawn data on the screen
        m_canvas->display();
    }

}