#include "canvas.hpp"

#include <iostream>
#include <cassert>
#include <cstring>
#include <cstdlib>

#include <chrono>
#include <ctime>
#include <thread>

namespace TE {

	Canvas::Canvas(int width, int height) :
		Image(width, height),
		m_buffer(new char[m_width + 1])
	{
		m_buffer[m_width] = '\0';
	}

	Canvas::Canvas(const Canvas& canvas) {
		m_width = canvas.m_width;
		m_height = canvas.m_height;

		const int size = m_width * m_height;
		m_data = new char[size + 1];
		std::memcpy(m_data, canvas.m_data, size);
		m_data[size] = '\0';

		m_buffer = new char[m_width + 1];
		std::memset(m_buffer, '0', m_width);
		m_buffer[m_width] = '\0';
	}

	Canvas::Canvas(Canvas&& canvas) {
		m_width = canvas.m_width;
		m_height = canvas.m_height;
		m_data = canvas.m_data;
		m_buffer = canvas.m_buffer;

		canvas.m_width = 0;
		canvas.m_height = 0;
		canvas.m_data = nullptr;
		canvas.m_buffer = nullptr;
	}

	Canvas::Canvas(const Image& image) {
		m_width = image.getWidth();
		m_height = image.getHeight();

		const int size = m_width * m_height;
		m_data = new char[size + 1];
		std::memcpy(m_data, image.getData(), size);
		m_data[size] = '\0';

		m_buffer = new char[m_width + 1];
		std::memset(m_buffer, '0', m_width);
		m_buffer[m_width] = '\0';
	}

	Canvas::~Canvas() {
		delete[] m_buffer;
	}

	const char& Canvas::at(int x, int y) const {
		x = x < 0 ? 0 : x >= m_width - 1 ? m_width - 1 : x;
		y = y < 0 ? 0 : y >= m_height - 1 ? m_height - 1 : y;

		return m_data[y * m_width + x];
	}

	char& Canvas::at(int x, int y) {
		x = x < 0 ? 0 : x >= m_width - 1 ? m_width - 1 : x;
		y = y < 0 ? 0 : y >= m_height - 1 ? m_height - 1 : y;

		return m_data[y * m_width + x];
	}

	void Canvas::clear() {
		system("clear");
		std::memset(m_data, ' ', m_width * m_height);
	}

	void Canvas::display() const {
		static const int size = m_width * m_height + 1;
		for (int i = 0; i < size; i += m_width) {
			std::memcpy(m_buffer, m_data + i, m_width);
			puts(m_buffer);
		}
	}

	Canvas& Canvas::operator=(const Canvas& canvas) {
		delete[] m_data;

		m_width = canvas.m_width;
		m_height = canvas.m_height;

		const int size = m_width * m_height;
		m_data = new char[size + 1];
		std::memcpy(m_data, canvas.m_data, size);

		return *this;
	}

	Canvas& Canvas::operator=(Canvas&& canvas) {
		delete[] m_data;
		delete[] m_buffer;

		m_width = canvas.m_width;
		m_height = canvas.m_height;
		m_data = canvas.m_data;
		m_buffer = canvas.m_buffer;

		canvas.m_width = 0;
		canvas.m_height = 0;
		canvas.m_data = nullptr;
		canvas.m_buffer = nullptr;

		return *this;
	}

	Canvas& Canvas::operator=(const Image& image) {
		delete m_data;

		m_width = image.getWidth();
		m_height = image.getHeight();

		const int size = m_width * m_height;
		m_data = new char[size + 1];
		std::memcpy(m_data, image.getData(), size);

		return *this;
	}

}