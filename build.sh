#!/bin/bash

rm -rf build lib
mkdir build
cd build

echo $1

if [[ $1 == '-bt' ]]
then
	cmake -DBUILD_TESTS=ON ..
else 
	cmake ..
fi

make -j8

if [[ $1 == '-bt' ]]
then
	make test
fi